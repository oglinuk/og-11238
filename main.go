package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strings"
	"sync"
	"time"
)

var (
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"
	err          error
	homeServices = make(map[int]int)
	wg           = &sync.WaitGroup{}
	active       []string
)

// runCmd is a reusable exec.Command wrapper
func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

func scan(addr string) bool {
	// Only care if dialing the port doesnt err
	// TODO: Improve ...
	_, err := net.Dial("tcp", addr)
	if err == nil {
		log.Printf("Service running on port %s ...", addr)
		return true
	}
	return false
}

// scanLocalhost ports
func scanLocalhost() []string {
	var active []string
	maxPort := 65535

	// No point in checking < 22 really, but leaving
	// at 2 until absolutely necessary
	for i := 2; i < maxPort; i++ {
		if _, exists := homeServices[i]; !exists {
			homeServices[i] = 0
		}

		// TODO: Scan concurretly, *and consistently*.
		localAddr := fmt.Sprintf("localhost:%d", i)
		if scan(localAddr) == true {
			active = append(active, localAddr)
		}
	}
	sort.Strings(active)

	return active
}

func checkExists(path string) error {
	log.Printf("Checking exists: %s ...", path)
	if _, err := os.Stat(path); err != nil {
		return err
	}
	return nil
}

func init() {
	// Ensure manifests directories exist; if not run `go run ./goscripts/genmanifests.go`
	// and generate defaults
	if err = checkExists(MANIFESTSDIR); err != nil {
		if err = runCmd("go", "run", "./goscripts/gen/genmanifests.go"); err != nil {
			log.Fatalf("genmanifests::runCmd::ERROR: %s", err.Error())
		}
	}

	// Run genservices to download/update all defined services in manifests/*
	if err = runCmd("go", "run", "./goscripts/gen/genservices.go"); err != nil {
		log.Printf("genservices::runCmd::ERROR: %s", err.Error())
	}
}

func main() {
	PORT := 11238
	HOST := "0.0.0.0"

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		sTime := time.Now()

		// scan ports to see which services are available
		// TODO:
		// add manifest of standard/reserved service ports to check initially
		// as a heartbeat analytics measure.
		log.Printf("Scanning localhost ...")

		// TODO: Improve ...
		activeLocalhostPorts := scanLocalhost()
		sort.Strings(activeLocalhostPorts)

		log.Println("Finished scan of localhost ...")

		var portLinks []string
		for _, port := range activeLocalhostPorts {
			portLinks = append(portLinks,
				fmt.Sprintf("<a href=\"http://%s\">%s</a>", port, strings.Split(port, ":")[1]))
		}

		// TODO: Refactor using a template
		fmt.Fprintf(w, `
			<h1>Hello %d!</h1><br><br>

			This is your <b><i>localhost: DONT PANIC!</i></b> A bazaar of
			services all running on various ports locally! Below is a list
			of all active ports. This page took %s to load.

			<h3>Current active ports:</h3><br>

			%v
		`, PORT, time.Since(sTime), portLinks)
	})

	http.Handle("/services", http.FileServer(http.Dir(SERVICESDIR)))

	log.Printf("Serving at localhost:%d ...", PORT)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%d", HOST, PORT), nil))
}
