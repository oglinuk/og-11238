package main

import (
	"os"
	"os/exec"
	"testing"
)

func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

func TestGenManifests(t *testing.T) {
	if err = runCmd("go", "run", "./genmanifests.go"); err != nil {
		t.Errorf("runCmd::ERROR: %s", err.Error())
	}

	if _, err := os.Stat("./manifests"); err != nil {
		t.Errorf("Expected os.Stat; Got ERROR: %s", err.Error())
	} else {
		os.RemoveAll("./manifests")
	}
}
