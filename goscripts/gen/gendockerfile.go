// +build ignore

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	err          error
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"

	// TODO: Abstract behind a struct and config file

	// Ex:
	// -n sbh-9001
	serviceName = flag.String("n", "", "Alias of the service")

	// -p 9001
	servicePort = flag.String("p", "", "Port that service runs on")
)

func init() {
	flag.Parse()
}

// TODO: Allow for more configurability
func main() {
	serviceSourcePath := fmt.Sprintf("%s/%s-%s", SERVICESDIR, *serviceName, *servicePort)

	// TODO: Abstract main.go generation to its own goscript
	log.Println(serviceSourcePath)
	dockerfile, err := os.Create(fmt.Sprintf("%s/Dockerfile", serviceSourcePath))
	if err != nil {
		log.Fatalf("os.Create(Dockerfile)::ERROR: %s", err.Error())
	}
	defer dockerfile.Close()

	fullServiceName := fmt.Sprintf("%s-%s", *serviceName, *servicePort)

	// TODO: Abstract behind a way to specify fill-in areas (ie FROM <base>)
	dockerfile.WriteString(fmt.Sprintf(`FROM golang:1.15
ADD . /go/src/%s
WORKDIR /go/src/%s
RUN go build -o %s-container
EXPOSE %s
CMD ["./%s-container"]`,
		fullServiceName, fullServiceName, fullServiceName,
		fmt.Sprintf("%s:%s", *servicePort, *servicePort), fullServiceName))
}
