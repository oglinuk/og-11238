// +build ignore

package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	err          error
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"

	// TODO: Abstract behind a struct and config file

	// Ex:
	// sbh-9001
	serviceName = flag.String("n", "", "Alias of the service")

	// 9001
	servicePort = flag.String("p", "", "Port that service runs on")

	// A list of freely available information
	//serviceDescription = flag.String("sd", "", "Description of the service")

)

func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

func init() {
	flag.Parse()

	f, err := os.OpenFile("gen.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("Could not open log file ...")
	}
	//defer f.Close()

	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)
}

// TODO: Allow for more configurability
func main() {
	log.Printf("[GENPANDOC] =========MAIN==========")
	// Walk over all services files/directories and check for *.md files
	err = filepath.Walk("services", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if strings.Contains(path, ".md") {
			//log.Printf("Found MD FILE: %s", path)
			mdfile := strings.Split(filepath.Base(path), ".")[0]
			//txtfile := fmt.Sprintf("%s.txt", mdfile) // TODO: Run pandoc on code snippets to make curlable/viewable
			htmlfile := fmt.Sprintf("%s.html", mdfile)
			if err = runCmd("pandoc", path, "-o", htmlfile); err != nil {
				log.Printf("genpandoc::runCmd::ERROR: %s", err.Error())
			} else {
				log.Printf("SUCCESSFULLY GENERATED %s ...", htmlfile)
			}
		}
		return nil
	})
	// Run `pandoc <filename>.md -o <filename>.html
}
