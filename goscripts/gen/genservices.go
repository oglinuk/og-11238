// +build ignore

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
)

var (
	err          error
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"
	wg           = &sync.WaitGroup{}
	sType        string
)

func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

func genService(sType, sPath string) {
	f, err := os.Open(sPath)
	if err != nil {
		// TODO: Improve ...
		log.Fatalf("genService::os.Open::ERROR: %s", err.Error())
	}
	defer f.Close()

	bs := bufio.NewScanner(f)

	for bs.Scan() {
		service := bs.Text()
		sc := strings.Split(service, " ")

		// Ex: services/sbh-9001
		toCheck := fmt.Sprintf("%s/%s-%s", SERVICESDIR, sc[0], sc[1])
		_, err = os.Stat(toCheck)
		if err != nil {
			log.Printf("[GENSERVICES] %s - %s ...", sType, toCheck)
			if err = runCmd("go", "run", "./goscripts/gen/genservice.go",
				"-n", sc[0], "-p", sc[1], "-l", sc[2], "-t", sType); err != nil {
				log.Printf("genservice::runCmd::ERROR: %s", err.Error())
			}
		} else {
			// TODO: `cd toCheck` && `git pull`
			//////////////////////////////////////////////////////log.Printf("Updating: %s ...", toCheck)
		}
	}
}

func init() {
	flag.Parse()

	// Ensure `services` directory exists
	if _, err = os.Stat(SERVICESDIR); err != nil {
		if err = os.MkdirAll(SERVICESDIR, 0744); err != nil {
			log.Fatalf("checkExists(%s)::ERROR: %s", SERVICESDIR, err.Error())
		}
	}

	f, err := os.OpenFile("gen.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("Could not open log file ...")
	}
	//defer f.Close()

	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)
}

func main() {
	// Get all manifest files
	manifests, err := ioutil.ReadDir(MANIFESTSDIR)
	if err != nil {
		//
		panic(err)
	}

	// For every manifest file, read its contents and download/update entries
	for _, manifest := range manifests {
		manifestName := manifest.Name()
		manifestPath := fmt.Sprintf("%s/%s", MANIFESTSDIR, manifestName)

		wg.Add(1)
		go func(mp, manifestName string) {
			defer wg.Done()
			if manifestName == "blogs" || manifestName == "docs" {
				log.Printf("[GENSERVICE ===FILESERVER===] %s ...", mp)
				genService("fileserver", mp)
			} else {
				log.Printf("[GENSERVICE ===%s===] %s ...", manifestName, mp)
				genService(manifestName, mp)
			}
		}(manifestPath, manifestName)
	}
	wg.Wait()
	log.Println("Success!")
}
