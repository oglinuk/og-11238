// +build ignore

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var (
	err          error
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"

	// TODO: Abstract behind a struct and config file

	// Ex:
	// -s sbh-9001
	serviceName = flag.String("n", "", "Alias of the service")

	// -p 9001
	servicePort = flag.String("p", "", "Port that service runs on")
)

func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

// Ensure a Dockerfile exists; if not `go run gendockerfile.go`
func init() {
	flag.Parse()

	log.Printf("[GENDOCKERFILE: %s %s", *serviceName, *servicePort)
	err = runCmd("go", "run", "./goscripts/gen/gendockerfile.go", "-n", *serviceName, "-p", *servicePort)
	if err != nil {
		log.Printf("genservice::runCmd::ERROR: %s", err.Error())
	}
}

func main() {
	serviceSourcePath := fmt.Sprintf("%s/%s-%s", SERVICESDIR, *serviceName, *servicePort)

	log.Printf("[GENFILESERVER] SCAFFOLDING main.go FOR %s ...", serviceSourcePath)
	// TODO: Abstract main.go generation to its own goscript
	maindotgo, err := os.Create(fmt.Sprintf("%s/main.go", serviceSourcePath))
	if err != nil {
		log.Fatalf("os.Create(%s/main.go)::ERROR: %s", serviceSourcePath, err.Error())
	}
	defer maindotgo.Close()

	maindotgo.WriteString(fmt.Sprintf(`package main

import (
	"fmt"
	"log"
	"net/http"
)

var (
	NAME = "%s"
	PORT = %s
)

func main() {
	http.Handle("/", http.FileServer(http.Dir(".")))
	log.Printf("Serving %s on %s ...", NAME, PORT)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%s", PORT), nil))
}`, *serviceName, *servicePort, "%s", "%d", "%d"))
}
