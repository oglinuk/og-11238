package main

import (
	"os"
	"testing"
)

func TestGenService(t *testing.T) {
	if err = os.MkdirAll("services", 0744); err != nil {
		t.Errorf("os.MkdirAll::ERROR: %s", err.Error())
	}
	if err = runCmd("go", "run", "./genservice.go", "-n", "sbh", "-p", "9001", "-l", "https://gitlab.com/oglinuk/sbh.git"); err != nil {
		t.Errorf("runCmd::ERROR: %s", err.Error())
	}

	if _, err := os.Stat("./services/sbh-9001"); err != nil {
		t.Errorf("Expected os.Stat; Got ERROR: %s", err.Error())
	} else {
		os.RemoveAll("./services")
		os.RemoveAll("./gen.log")
	}
}
