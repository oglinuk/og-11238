package main

import (
	"fmt"
	"log"
	"os"
	"sync"
)

var (
	err          error
	wg           = &sync.WaitGroup{}
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"

	manifestTypes = []string{
		"functions",
		"applications",
		"blogs",
		"docs",
		"personal",
		"news",
	}

	// A function is a stateless utility that helps with one or more problems
	// Ex: sbh - stateless password manager
	functionsDefault = []string{
		"sbh 9001 https://gitlab.com/oglinuk/sbh.git",
		"quotitioner 12321 https://gitlab.com/oglinuk/quotitioner.git",
	}

	// An application is a piece of software that requires state
	// Ex: gitea - self host git services && web ui
	applicationsDefault = []string{
		"gitea 3000 https://github.com/go-gitea/gitea.git", // TODO: Fix (to 9090) when gendockercompose is implemented
		//"owncast https://github.com/owncast/owncast.git 11211" // TODO: ...

	}

	// A blog is a git hosted collection of .md/.html/.txt files about any topic
	// Ex: rwx.gg - beginner boosts
	blogsDefault = []string{
		"rwx.gg 50007 https://gitlab.com/rwx.gg/README.git",
		"drummyfish 50050 https://gitlab.com/drummyfish/my_writings.git",
		"jessfraz 50100 https://github.com/jessfraz/blog.git",
	}

	// A doc is a git hosted collection of code/.md/.html/.txt/... files
	// documenting something
	// Ex: architecture-patterns - design patterns
	docsDefault = []string{
		"golang-training 50015 https://github.com/GoesToEleven/GolangTraining.git",
		"golang-webdev 50016 https://github.com/GoesToEleven/golang-web-dev.git",
		"postgresql-course 50017 https://github.com/GoesToEleven/postgresql-course.git",
		"go-hardcore-training 50018 https://github.com/GoesToEleven/gotraining.git",
		"architecture-patterns 51015 https://github.com/chanakaudaya/solution-architecture-patterns.git",
	}

	// A personal source is ones creation(s). They can be any type of service
	personalDefault = []string{
		//"fourohfournotfound 40404 https://gitlab.com/oglinuk/fourohfournotfound.git"
	}

	// A news source is a collection of information either stored in a git
	// repo or available via some way (like REST)
	// Ex: newshole - reddit, news.ycombinator, ... aggregator
	newsDefault = []string{
		//"newshole https://gitlab.com/oglinuk/newshole.git 55000",
	}
)

func createManifestFile(name string, defaults []string) error {
	f, err := os.Create(fmt.Sprintf("%s/%s", MANIFESTSDIR, name))
	if err != nil {
		return err
	}
	defer f.Close()

	for _, d := range defaults {
		f.WriteString(fmt.Sprintf("%s\n", d))
	}
	return nil
}

// Generate a manifest default of manifestType
func generateManifest(manifestType string) error {
	switch manifestType {
	case "functions":
		return createManifestFile(manifestType, functionsDefault)
	case "applications":
		return createManifestFile(manifestType, applicationsDefault)
	case "blogs":
		return createManifestFile(manifestType, blogsDefault)
	case "docs":
		return createManifestFile(manifestType, docsDefault)
	case "personal":
		return createManifestFile(manifestType, personalDefault)
	case "news":
		return createManifestFile(manifestType, newsDefault)
	default:
		return fmt.Errorf("%s::ERROR: Unknown type", manifestType)
	}
}

func createDefaultManifests() error {
	for _, t := range manifestTypes {
		if err = generateManifest(t); err != nil {
			log.Printf("generateManifest(%s)::ERROR: %s", t, err.Error())
			return err
		}
	}

	return nil
}

func createManifestsDir() error {
	if err = os.MkdirAll(MANIFESTSDIR, 0744); err != nil {
		return err
	}
	if err = createDefaultManifests(); err != nil {
		return err
	}
	return nil
}

// Create manifests directory && create standard default manifest files
// Ex:
/*
	/manifests
		|- functions
			|- sbh https://gitlab.com/oglinuk/sbh.git 9001
			|- quotitioner https://gitlab.com/oglinuk/quotitioner.git 12321
		|- applications
			|- gitea https://github.com/go-gitea/gitea.git 9090
		|- blogs
			|- rwx.gg https://gitlab.com/rwx.gg/README.git 50007
			|- drummyfish https://gitlab.com/drummyfish/my_writings.git 50050
			|- jessfraz https://github.com/jessfraz/blog.git 50100
		|- docs
			|- golang-training https://github.com/GoesToEleven/GolangTraining.git 50015
			|- golang-webdev https://github.com/GoesToEleven/golang-web-dev.git 50016
			|- postgresql-course https://github.com/GoesToEleven/postgresql-course.git 50017
			|- go-hardcore-training https://github.com/GoesToEleven/gotraining.git 50018
			|- architecture-patterns https://github.com/chanakaudaya/solution-architecture-patterns.git 51015
		|- personal
			|- ...
		|- news
			|- TODO: ...
*/
func main() {
	if err = createManifestsDir(); err != nil {
		log.Fatalf("createManifestDir::ERROR: %s", err.Error())
	}
}
