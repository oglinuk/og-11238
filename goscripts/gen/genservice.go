// +build ignore

package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"sync"
)

var (
	err          error
	MANIFESTSDIR = "manifests"
	SERVICESDIR  = "services"
	wg           = &sync.WaitGroup{}

	// TODO: Abstract behind a struct and config file

	// Ex:
	// sbh-9001
	serviceName = flag.String("n", "", "Alias of the service")

	// 9001
	servicePort = flag.String("p", "", "Port that service runs on")

	// TODO: Implement genreadme.go
	// "A stateless password manager for all your passwords."
	//serviceDescription = flag.String("d", "", "Description of the service")

	// function
	serviceType = flag.String("t", "", "Type of service for scaffolding: function, blog, ect ...")

	// https://gitlab.com/oglinuk/sbh
	serviceLink = flag.String("l", "", "Link o git repository of service")
)

func runCmd(cmd string, args ...string) error {
	execCmd := exec.Command(cmd, args...)
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr

	if err = execCmd.Run(); err != nil {
		return err
	}
	return nil
}

func init() {
	flag.Parse()

	f, err := os.OpenFile("gen.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("Could not open log file ...")
	}
	//defer f.Close()

	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)
}

func main() {
	serviceSourcePath := fmt.Sprintf("%s/%s-%s", SERVICESDIR, *serviceName, *servicePort)

	// If serviceLink; then `git clone serviceLink serviceSourcePath`
	if *serviceLink != "" {
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err = runCmd("git", "clone", *serviceLink, serviceSourcePath); err != nil {
				log.Printf("genservice::runCmd::gitclone::ERROR: %s", err.Error())
			}
		}()
		wg.Wait()
	}

	// If serviceType is fileserver, then genfileserver
	if *serviceType == "fileserver" {
		if err = runCmd("go", "run", "./goscripts/gen/genfileserver.go", "-n", *serviceName, "-p", *servicePort); err != nil {
			log.Printf("genfileserver::runCmd::ERROR: %s", err.Error())
		}
	}
}
